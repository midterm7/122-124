import 'dart:io';
import 'dart:math';
// 138*+
// 25

int precedence(String ch) {
  if (ch == '+' || ch == '-') {
    return 1;
  } else if (ch == '*' || ch == '/') {
    return 2;
  } else if (ch == '^') {
    return 3;
  } else {
    return 0;
  }
}

List toPostfix(List<String> infix) {
  var operators = [];
  var postfix = [];

  for (int i = 0; i < infix.length; i++) {
    if (RegExp(r'^[A-za-z0-9]+$').hasMatch(infix[i])) {
      postfix.add(infix[i]);
    } else if (["*", "/", "^", "+", "-"].contains(infix[i])) {
      while (operators.length != 0 &&
          operators.last == "(" &&
          precedence(postfix[i]) < precedence(operators.last)) {
        var tmp = operators.last;
        operators.removeAt(operators.length - 1);
        postfix.add(tmp);
      }
      operators.add(infix[i]);
    } else if (infix[i] == "(") {
      operators.add(infix[i]);
    } else if (infix[i] == ")") {
      while (operators.last != "(") {
        var tmp = operators.last;
        operators.removeAt(operators.length - 1);
        postfix.add(tmp);
      }
      operators.removeAt(operators.length - 1);
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.last);
    operators.removeAt(operators.length - 1);
  }
  return postfix;
}

double evalPostfix(List postfix) {
  var values = [];

  for (int i = 0; i < postfix.length; i++) {
    if (RegExp(r'^[A-za-z0-9]+$').hasMatch(postfix[i])) {
      values.add(double.parse(postfix[i]));
    } else {
      double right = values.last;
      values.removeAt(values.length - 1);
      double left = values.last;
      values.removeAt(values.length - 1);
      if (postfix[i] == "+") {
        values.add(left + right);
      } else if (postfix[i] == "-") {
        values.add(left - right);
      } else if (postfix[i] == "*") {
        values.add(left * right);
      } else if (postfix[i] == "/") {
        values.add(left / right);
      } else if (postfix[i] == "^") {
        values.add(pow(left, right));
      }
    }
  }
  return values.first;
}

void main() {
  print('Enter String: ');
  String str = stdin.readLineSync()!;
  print(evalPostfix(str.split('')));
}

// 138*+
// 25