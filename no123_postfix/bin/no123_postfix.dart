import 'dart:io';

// A+B*C
// A B C * +

List<String> tokenize(str) {
  str = str.split('');
  int max = str.length - 1;
  int i = 0;
  while (i <= max) {
    if (["*", "/", "^", "(", ")"].contains(str[i])) {
      i++;
    } else if (str[i] == " ") {
      str.removeAt(i);
      max--;
    } else if (["+", "-"].contains(str[i])) {
      if (RegExp(r'^[A-za-z0-9)]+$').hasMatch(str[i - 1])) {
        i++;
      } else {
        str[i + 1] = str[i] + str[i + 1];
        str.removeAt(i);
        max--;
      }
    } else {
      if (i == 0) {
        i++;
      } else if (RegExp(r'^[A-za-z0-9]+$').hasMatch(str[i - 1])) {
        str[i] = str[i - 1] + str[i];
        str.removeAt(i - 1);
        max--;
        i--;
      } else {
        i++;
      }
    }
  }
  return str;
}

int precedence(String ch) {
  if (ch == '+' || ch == '-') {
    return 1;
  } else if (ch == '*' || ch == '/') {
    return 2;
  } else if (ch == '^') {
    return 3;
  } else {
    return 0;
  }
}

List toPostfix(List<String> infix) {
  var operators = [];
  var postfix = [];

  for (int i = 0; i < infix.length; i++) {
    if (RegExp(r'^[A-za-z0-9]+$').hasMatch(infix[i])) {
      postfix.add(infix[i]);
    } else if (["*", "/", "^", "+", "-"].contains(infix[i])) {
      while (operators.length != 0 &&
          operators.last == "(" &&
          precedence(postfix[i]) < precedence(operators.last)) {
        var tmp = operators.last;
        operators.removeAt(operators.length - 1);
        postfix.add(tmp);
      }
      operators.add(infix[i]);
    } else if (infix[i] == "(") {
      operators.add(infix[i]);
    } else if (infix[i] == ")") {
      while (operators.last != "(") {
        var tmp = operators.last;
        operators.removeAt(operators.length - 1);
        postfix.add(tmp);
      }
      operators.removeAt(operators.length - 1);
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.last);
    operators.removeAt(operators.length - 1);
  }
  return postfix;
}

void main(List<String> arguments) {
  print('Enter String: ');
  String str = stdin.readLineSync()!;
  var infix = (tokenize(str));
  print(toPostfix(infix));
}

// A+B*C
// A B C * +