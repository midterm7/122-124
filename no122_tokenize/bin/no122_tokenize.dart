import 'dart:io';

// (A7*4) - (Z3+8) -  ( B6 / 11)
// ['(', 'A7', '*', '4', ')', '-', '(', 'Z3', '+', '8', ')', '-', '(', 'B6', '/', '11', ')']

List<String> tokenize(str) {
  str = str.split('');
  int max = str.length - 1;
  int i = 0;
  while (i <= max) {
    if (["*", "/", "^", "(", ")"].contains(str[i])) {
      i++;
    } else if (str[i] == " ") {
      str.removeAt(i);
      max--;
    } else if (["+", "-"].contains(str[i])) {
      if (RegExp(r'^[A-za-z0-9)]+$').hasMatch(str[i - 1])) {
        i++;
      } else {
        str[i + 1] = str[i] + str[i + 1];
        str.removeAt(i);
        max--;
      }
    } else {
      if (i == 0) {
        i++;
      } else if (RegExp(r'^[A-za-z0-9]+$').hasMatch(str[i - 1])) {
        str[i] = str[i - 1] + str[i];
        str.removeAt(i - 1);
        max--;
        i--;
      } else {
        i++;
      }
    }
  }
  return str;
}

void main() {
  print('Enter String: ');
  String? str = stdin.readLineSync();
  print(tokenize(str));
}

// (A7*4) - (Z3+8) -  ( B6 / 11)
// ['(', 'A7', '*', '4', ')', '-', '(', 'Z3', '+', '8', ')', '-', '(', 'B6', '/', '11', ')']
